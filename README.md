# Docker Compose and WordPress

Step 1: Clone this repo

Step 2: Run Docker Compose in detached mode
        docker-compose up -d

Step 3: Navigate to http://localhost:8000

Step 4: Do installation of WordPress, The docker volume db_data persists any updates made by WordPress to the database.

Step 5: To Stop and clean the voulume use following Command
        docker-compose down
        docker-compose down --volumes

        
